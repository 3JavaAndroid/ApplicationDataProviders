package sda3.amens.com.moviesdatabase.db;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import sda3.amens.com.moviesdatabase.model.Movie;

/**
 * Created by amen on 5/30/17.
 */

public class SharedPreferencesDataProvider implements IDataProvider {
    private static final String PREFERENCES_PATH = "prefs_file";
    private static final String KEY_MOVIES = "movies_key";

    // shared preferences
    private SharedPreferences sharedPreferences;

    public SharedPreferencesDataProvider(Context context) {
        sharedPreferences = context.getSharedPreferences(PREFERENCES_PATH, Context.MODE_PRIVATE);
    }

    @Override
    public List<Movie> getAllMovies() {
        // tworzymy pustą listę filmów
        List<Movie> returnedMoviesList = new ArrayList<>();

        // pobieramy zbiór stringów z shared preferences
        Set<String> moviesSet = sharedPreferences.getStringSet(KEY_MOVIES, new HashSet<String>());

        // iterujemy każdy string i parsujemy je do obiektów typu Movie
        // i dodajemy film do listy
        for(String singleMovie : moviesSet){
            try {
                returnedMoviesList.add(new Movie(singleMovie));
            }catch (ParseException ex){
                Log.e(getClass().getName(), "Couldn't parse date from: " +singleMovie);
            }
        }

        // zwracamy filmy
        return returnedMoviesList;
    }

    @Override
    public void saveAllMovies(List<Movie> list) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Set<String> collectionOfMovies = new HashSet<>();

        for(Movie movie : list){
            collectionOfMovies.add(movie.toDatabaseFormat());
        }

        editor.putStringSet(KEY_MOVIES, collectionOfMovies);
        editor.commit();
    }
}
