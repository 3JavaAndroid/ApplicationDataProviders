package sda3.amens.com.moviesdatabase.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by amen on 5/30/17.
 */

public class Movie {
    // separator for db format
    private static final String SEPARATOR = ";";

    // fields
    private int id = -1;
    private String title;
    private String graphicsURL;
    private Date releaseDate;
    private int duration;
    private int watchedBy;
    private boolean seen;

    public Movie(){
    }

    public Movie(String title, int id, String graphicsURL, Date releaseDate, int duration, int watchedBy, boolean seen) {
        this.title = title;
        this.id = id;
        this.graphicsURL = graphicsURL;
        this.releaseDate = releaseDate;
        this.duration = duration;
        this.watchedBy = watchedBy;
        this.seen = seen;
    }

    public Movie(String unparsedString) throws ParseException {
        String[] splits = unparsedString.split(SEPARATOR);

        // splits[0] - title
        this.title = splits[0];

        // splits[1] - id
        this.id = Integer.parseInt(splits[1]);

        // splits[2] - graphics URL
        this.graphicsURL = splits[2];

        // splits[3] - releaseDate
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        this.releaseDate = sdf.parse(splits[3]);

        // splits[4] - duration
        this.duration = Integer.parseInt(splits[4]);

        // splits[5] - watchedBy
        this.watchedBy = Integer.parseInt(splits[5]);

        // splits[6] - seen
        this.seen = Boolean.parseBoolean(splits[6]);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGraphicsURL() {
        return graphicsURL;
    }

    public void setGraphicsURL(String graphicsURL) {
        this.graphicsURL = graphicsURL;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getWatchedBy() {
        return watchedBy;
    }

    public void setWatchedBy(int watchedBy) {
        this.watchedBy = watchedBy;
    }

    public boolean isSeen() {
        return seen;
    }

    public void setSeen(boolean seen) {
        this.seen = seen;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "title='" + title + '\'' +
                ", id=" + id +
                ", graphicsURL='" + graphicsURL + '\'' +
                ", releaseDate=" + releaseDate +
                ", duration=" + duration +
                ", watchedBy=" + watchedBy +
                ", seen=" + seen +
                '}';
    }

    public String toDatabaseFormat() {
        StringBuilder builder = new StringBuilder();

        builder.append(title);

        builder.append(SEPARATOR);
        builder.append(id);

        builder.append(SEPARATOR);
        builder.append(graphicsURL);

        builder.append(SEPARATOR);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        builder.append(sdf.format(releaseDate));

        builder.append(SEPARATOR);
        builder.append(duration);

        builder.append(SEPARATOR);
        builder.append(watchedBy);

        builder.append(SEPARATOR);
        builder.append(seen);

        return builder.toString();
    }
}
