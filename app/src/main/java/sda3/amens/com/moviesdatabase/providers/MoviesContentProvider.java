package sda3.amens.com.moviesdatabase.providers;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import sda3.amens.com.moviesdatabase.db.InternalDatabaseDataProvider;

/**
 * Created by amen on 6/8/17.
 */

public class MoviesContentProvider extends ContentProvider {
    // Nazwa providera
    private static final String PROVIDER_NAME =
            "sda3.amen.com.moviesdatabase.providers.MoviesContentProvider";
    // URI naszego providera
    private static final String PROVIDER_URI =
            "content://" + PROVIDER_NAME + "/movies";

    //
    private static final Uri uri = Uri.parse(PROVIDER_URI);

    /**
     * Weryfikujemy dostęp do bazy danych poprzez stworzenie i pobranie dostępnej bazy danych.
     *
     * @return - on success true
     */
    @Override
    public boolean onCreate() {
        Context context = getContext();
        InternalDatabaseDataProvider provider = new InternalDatabaseDataProvider(context);
        SQLiteDatabase db = provider.getReadableDatabase();

        if (db != null) {
            return true;
        }
        return false;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        // pobieramy bazę danych
        InternalDatabaseDataProvider provider = new InternalDatabaseDataProvider(getContext());
        SQLiteDatabase db = provider.getReadableDatabase();

        // wywołujemy zapytanie
        Cursor cursor = db.query(InternalDatabaseDataProvider.getDbTableName(),
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder);

        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        // pobieramy bazę danych
        InternalDatabaseDataProvider provider = new InternalDatabaseDataProvider(getContext());
        SQLiteDatabase db = provider.getReadableDatabase();

        long id = db.insert(InternalDatabaseDataProvider.getDbTableName(),
                null,
                values);

        return Uri.parse(PROVIDER_URI + "/" + id);
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        // pobieramy bazę danych
        InternalDatabaseDataProvider provider = new InternalDatabaseDataProvider(getContext());
        SQLiteDatabase db = provider.getReadableDatabase();

        return db.delete(InternalDatabaseDataProvider.getDbTableName(),
                selection,
                selectionArgs);
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        // pobieramy bazę danych
        InternalDatabaseDataProvider provider = new InternalDatabaseDataProvider(getContext());
        SQLiteDatabase db = provider.getReadableDatabase();

        return db.update(InternalDatabaseDataProvider.getDbTableName(),
                values,
                selection,
                selectionArgs);
    }
}
