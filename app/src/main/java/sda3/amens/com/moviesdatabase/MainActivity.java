package sda3.amens.com.moviesdatabase;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import butterknife.ButterKnife;
import butterknife.OnClick;
import sda3.amens.com.moviesdatabase.model.ProviderType;

public class MainActivity extends AppCompatActivity {

    @OnClick(R.id.buttonAddInternalDB)
    protected void addInternalDB() {
        Intent i = new Intent(this, AddMovieActivity.class);
        // przekazujemy typ providera
        i.putExtra("PROVIDER_TYPE", ProviderType.SQLITE.toString());

        // uruchamiamy activity
        startActivity(i);
    }

    @OnClick(R.id.buttonAddInternalStorage)
    protected void addInternalStorage() {
        Intent i = new Intent(this, AddMovieActivity.class);
        // przekazujemy typ providera
        i.putExtra("PROVIDER_TYPE", ProviderType.INTERNAL_STORAGE.toString());

        // uruchamiamy activity
        startActivity(i);
    }

    @OnClick(R.id.buttonAddSharedPreferences)
    protected void addSharedPreferences() {
        Intent i = new Intent(this, AddMovieActivity.class);
        // przekazujemy typ providera
        i.putExtra("PROVIDER_TYPE", ProviderType.SHARED_PREFERENCES.toString());

        // uruchamiamy activity
        startActivity(i);
    }

    @OnClick(R.id.buttonAddExternalStorage)
    protected void addExternalStorage() {
        Intent i = new Intent(this, AddMovieActivity.class);
        // przekazujemy typ providera
        i.putExtra("PROVIDER_TYPE", ProviderType.EXTERNAL_STORAGE.toString());

        // uruchamiamy activity
        startActivity(i);
    }

    @OnClick(R.id.buttonViewInternalDB)
    protected void viewInternalDB() {
        Intent i = new Intent(this, ViewMoviesActivity.class);
        // przekazujemy typ providera
        i.putExtra("PROVIDER_TYPE", ProviderType.SQLITE.toString());

        // uruchamiamy activity
        startActivity(i);
    }

    @OnClick(R.id.buttonViewInternalStorage)
    protected void viewInternalStorage() {
        Intent i = new Intent(this, ViewMoviesActivity.class);
        // przekazujemy typ providera
        i.putExtra("PROVIDER_TYPE", ProviderType.INTERNAL_STORAGE.toString());

        // uruchamiamy activity
        startActivity(i);
    }

    @OnClick(R.id.buttonViewSharedPreferences)
    protected void viewSharedPreferences() {
        Intent i = new Intent(this, ViewMoviesActivity.class);
        // przekazujemy typ providera
        i.putExtra("PROVIDER_TYPE", ProviderType.SHARED_PREFERENCES.toString());

        // uruchamiamy activity
        startActivity(i);
    }

    @OnClick(R.id.buttonViewExternalStorage)
    protected void viewExternalStorage() {
        Intent i = new Intent(this, ViewMoviesActivity.class);
        // przekazujemy typ providera
        i.putExtra("PROVIDER_TYPE", ProviderType.EXTERNAL_STORAGE.toString());

        // uruchamiamy activity
        startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        isStoragePermissionGranted();
    }

    public void isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                // uprawnienia wcześniej przyznane
                Log.d(getClass().getName(), "Permission already granted");
            } else {
                Log.d(getClass().getName(), "Requesting permission");
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        1);
            }
        } else {
            Log.d(getClass().getName(), "Permission already granted");
        }
    }
}
