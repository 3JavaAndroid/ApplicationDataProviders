package sda3.amens.com.moviesdatabase.model;

/**
 * Created by amen on 5/30/17.
 */

public enum ProviderType {
    SHARED_PREFERENCES(1),
    EXTERNAL_STORAGE(2),
    INTERNAL_STORAGE(3),
    SQLITE(4);

    int value;

    ProviderType(int i) {
        value = i;
    }

    public int getValue() {
        return value;
    }
}
