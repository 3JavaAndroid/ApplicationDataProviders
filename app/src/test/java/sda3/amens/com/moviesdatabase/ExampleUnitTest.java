package sda3.amens.com.moviesdatabase;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import sda3.amens.com.moviesdatabase.model.Movie;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(Parameterized.class)
public class ExampleUnitTest {
    private String line;
    private String title;

    public ExampleUnitTest (String line, String titles){
        this.line = line;
        this.title = titles;
    }

    @Parameterized.Parameters
    public static Collection MovieStrings(){
        return Arrays.asList(new Object[][]{
                {"title;1;http;2013/01/01;1;1;true", "title"},
                {"title2;1;http;2013/01/01;1;1;true", "title2"},
                {"title3;1;http;2013/01/01;1;1;true", "title3"}
        });
    }

    @Test
    public void testConstructor() throws ParseException{
        Movie m = new Movie(line);
        System.out.println("Testing: " + line);
        assertEquals(title, m.getTitle());
    }
}