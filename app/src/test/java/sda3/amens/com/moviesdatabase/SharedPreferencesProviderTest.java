package sda3.amens.com.moviesdatabase;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import sda3.amens.com.moviesdatabase.db.SharedPreferencesDataProvider;
import sda3.amens.com.moviesdatabase.model.Movie;

/**
 * Created by amen on 6/28/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class SharedPreferencesProviderTest {

    @Test
    public void testSharedPreferencesGet() {
        SharedPreferencesDataProvider provider =
                Mockito.mock(SharedPreferencesDataProvider.class);

        Mockito.when(provider.getAllMovies()).thenReturn(prepareMovies());

        Assert.assertEquals(provider.getAllMovies().size(), 3);
    }

    private List<Movie> prepareMovies() {
        List<Movie> listOfMovies = new LinkedList<>();

        listOfMovies.add(new Movie("title1",
                -1,
                "url",
                new Date(2013, 03, 03),
                1,
                2,
                true));
        listOfMovies.add(new Movie("title2",
                5,
                "url_2",
                new Date(2011, 02, 9),
                3,
                5,
                true));
        listOfMovies.add(new Movie("title3",
                2,
                "url_3",
                new Date(2015, 06, 3),
                5,
                7,
                false));

        return listOfMovies;
    }
}
