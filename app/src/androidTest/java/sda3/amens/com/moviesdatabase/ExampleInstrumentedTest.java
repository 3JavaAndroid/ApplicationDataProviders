package sda3.amens.com.moviesdatabase;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.Espresso;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.filters.SmallTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import sda3.amens.com.moviesdatabase.model.Movie;

import static org.junit.Assert.*;

import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.withDecorView;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
@SmallTest
public class ExampleInstrumentedTest {


    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(
            MainActivity.class);

    @Test
    public void useAppContext() throws Exception {
        String title = "titleMovie";
        String URL = "URL";
        Integer duration = 2;
        Integer seenTimes = 3;
        String inputDate = "1999/10/21";
        Date releaseDate = new SimpleDateFormat("yyyy/MM/dd").parse(inputDate);

        Espresso.onView(ViewMatchers.withId(R.id.buttonAddSharedPreferences)).perform(ViewActions.click());
        Espresso.onView(ViewMatchers.withId(R.id.editTitle)).perform(ViewActions.typeText(title), ViewActions.closeSoftKeyboard());
        Espresso.onView(ViewMatchers.withId(R.id.editUrl)).perform(ViewActions.typeText(URL), ViewActions.closeSoftKeyboard());
        Espresso.onView(ViewMatchers.withId(R.id.editRelease)).perform(ViewActions.typeText(inputDate.toString()), ViewActions.closeSoftKeyboard());
        Espresso.onView(ViewMatchers.withId(R.id.editDuration)).perform(ViewActions.typeText(duration.toString()), ViewActions.closeSoftKeyboard());
        Espresso.onView(ViewMatchers.withId(R.id.editSeenTimes)).perform(ViewActions.typeText(seenTimes.toString()), ViewActions.closeSoftKeyboard());
        Espresso.onView(ViewMatchers.withId(R.id.checkSeen)).perform(ViewActions.click());
        Espresso.onView(ViewMatchers.withId(R.id.buttonSave)).perform(ViewActions.click());
        Espresso.onView(ViewMatchers.withId(R.id.buttonViewSharedPreferences)).perform(ViewActions.click());

        Movie movie = new Movie(title, -1, URL, releaseDate, duration, seenTimes, true);
        String matched = movie.toString();
        Espresso.onView(ViewMatchers.withText(matched)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()));

    }

    @Test
    public void testWrongDate() throws Exception {
        String title = "titleMovie";
        String URL = "URL";
        Integer duration = 2;
        Integer seenTimes = 3;
        String inputDate = "1999-10-21";

        Random r = new Random(1287461287973L);
        int value = r.nextInt();
        System.out.println(value);
        value = r.nextInt();
        System.out.println(value);
//-555569545
        //-1845777997
        Espresso.onView(ViewMatchers.withId(R.id.buttonAddSharedPreferences)).perform(ViewActions.click());
        Espresso.onView(ViewMatchers.withId(R.id.editTitle)).perform(ViewActions.typeText(title), ViewActions.closeSoftKeyboard());
        Espresso.onView(ViewMatchers.withId(R.id.editUrl)).perform(ViewActions.typeText(URL), ViewActions.closeSoftKeyboard());
        Espresso.onView(ViewMatchers.withId(R.id.editRelease)).perform(ViewActions.typeText(inputDate.toString()), ViewActions.closeSoftKeyboard());
        Espresso.onView(ViewMatchers.withId(R.id.editDuration)).perform(ViewActions.typeText(duration.toString()), ViewActions.closeSoftKeyboard());
        Espresso.onView(ViewMatchers.withId(R.id.editSeenTimes)).perform(ViewActions.typeText(seenTimes.toString()), ViewActions.closeSoftKeyboard());
        Espresso.onView(ViewMatchers.withId(R.id.checkSeen)).perform(ViewActions.click());
        Espresso.onView(ViewMatchers.withId(R.id.buttonSave)).perform(ViewActions.click());

        Espresso.onView(withText("Wrong date format! Use yyyy/MM/dd."))
                .inRoot(withDecorView(not(is(mActivityRule.getActivity().getWindow().getDecorView()))))
                .check(matches(isDisplayed()));

    }

    @Test
    public void testMockito() throws Exception {

    }
}












